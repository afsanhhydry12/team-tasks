# Team Tasks

Team tasks specific to the Search team, including onboarding templates.

The Global Search Team homepage can be found in the handbook here: https://about.gitlab.com/handbook/engineering/development/enablement/search/