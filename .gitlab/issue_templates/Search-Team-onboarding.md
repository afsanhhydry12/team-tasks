## [First Name], Welcome to GitLab and the Global Search Team!

We are all excited that you are joining us on the Global Search Team.  You should have already received an onboarding issue from People Ops familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Global Search Team people, processes and setup.
For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Global Search team.
Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day
* [ ] Manager: Invite team member to #g_global_search Slack Channel
* [ ] Manager: Invite team member to weekly Team Meeting
* [ ] Manager: Add new team member to Global Search Team Retro


### First Week
* [ ] Manager: Add new team member to [geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Add new team member introduction to the [Engineering Week In Review](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit).
* [ ] New team member: Read about your team, its mission, team members and resources on the [Global Search Team page](https://about.gitlab.com/handbook/engineering/development/enablement/search/)
* [ ] New team member: Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet your team

### Second Week
* [ ] New team member: Read about how Gitlab uses labels [Issue Workflow Labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#issues-workflow), [Labels FOSS](https://gitlab.com/gitlab-org/gitlab-foss/-/labels)
* [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
* [ ] New team member: Familiarize yourself with the team boards
  * [ ] [Global Search Cross-Functional Prioritization](https://gitlab.com/groups/gitlab-org/-/boards/4416712?label_name[]=group%3A%3Aglobal%20search)
  * [ ] [Global Search Workflow Build: Plan](https://gitlab.com/groups/gitlab-org/-/boards/4440461?milestone_title=Started&label_name[]=Deliverable&label_name[]=group%3A%3Aglobal%20search)
  * [ ] [Global Search Workflow Build: Develop & Test](https://gitlab.com/groups/gitlab-org/-/boards/4440461?milestone_title=Started&label_name[]=Deliverable&label_name[]=group%3A%3Aglobal%20search)
  
* [ ] New team member: Please review this onboarding issue and update the template with some improvements as you see fit to make it easier for the next newbie!

### Processes
#### Engineering Workflow

* [Engineering Workflow](https://about.gitlab.com/handbook/engineering/workflow/)
* [Cross-Functional Prioritization](https://about.gitlab.com/handbook/product/product-processes/#cross-functional-prioritization)
* [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/)

#### Important Dates

* [Important Dates PMs Should Keep In Mind](https://about.gitlab.com/handbook/product/product-manager-role/#important-dates-pms-should-keep-in-mind)
* [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

#### Workflow Labels

* [Workflow Labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels)

#### Merge Request Workflow

* [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html)

#### Developing with Feature Flags
Consider using feature flags for every medium size-feature:

* [Feature flags in the development of GitLab](https://docs.gitlab.com/ee/development/feature_flags/)
* [Rolling out changes](https://docs.gitlab.com/ee/development/feature_flags/controls.html#rolling-out-changes)


#### Testing Best Practices

* [Testing Best Practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html)

### Youtube Playlist

* [Youtube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kqua2RYfzPFGqGtAHVOVhby)

### Learning Materials

* [Gitlab ElasticSearch Knowledge page](https://docs.gitlab.com/ee/development/elasticsearch.html)
* [Gitlab ElasticSearch Integration](https://docs.gitlab.com/ee/integration/elasticsearch.html)
* [GitLab ElasticSearch integration video](https://www.youtube.com/watch?v=vrvl-tN2EaA)
* [GitLab ElasticSearch integration Google Doc](https://docs.google.com/document/d/1cwo5n3XYaTDAJ48sMZJ8bHQVJ0RD5dlsdf28L96OZQw/edit?pli=1#)
* [Blog post about Elasticsearch on GitLab.com](https://about.gitlab.com/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [2020-04-14 Search Walkthrough](https://docs.google.com/presentation/d/1RK-Ji_OS7_v-cgmpoPLhdVk_iVlw065rjodMhrXCthA/edit?usp=sharing)
* [Advanced Global Search Performance Monitoring Deep Dive](https://docs.google.com/document/d/1U3jg6G0EmDsMmwm2tFba4rPMEvelOtBFrw6Nl8JvsHc/edit#)
* [ECS IAM Role based Authentication Demo](https://docs.google.com/document/d/1L2dz4ZlSZchpGdqIhEW-HjRy-8Idj_lITiiDg9W08lY/edit)

---

#### Getting Started Tasks

Here are a couple tasks that I’ve found that you can take on when you’re ready to start getting in to code. These should be fairly straight forward, but if they turn out to be too complex let me know. These first tasks aren’t intended to be complex features, they are intended to be simple tasks that will help you get familiar the process of shipping code at GitLab.

[add 2 to 3 getting started issues]

/confidential
/due in 21 days
